terraform {
  required_providers {
    aws = {
      region = "ap-south-1"
      access_key = ""
      secret_key = ""  
      source = "hashicorp/aws"
      version = "3.68.0"
    }
  }
}

provider "aws" {
  # Configuration options
}